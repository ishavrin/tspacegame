package com.android_ast.tspacegame.model

class Player(var x: Int, var y: Int) {

    companion object {
        val image: Array<CharArray> = arrayOf(
            "   o   ".toCharArray(),
            "  ooo  ".toCharArray(),
            "  ooo  ".toCharArray(),
            "oo o oo".toCharArray(),
            "\"\" \" \"\"".toCharArray()
        )
    }
}