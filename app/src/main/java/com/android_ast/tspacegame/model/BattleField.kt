package com.android_ast.tspacegame.model

class BattleField(val width: Int, val height: Int) {
    var image: Array<CharArray> = Array(width) { CharArray(height) { ' ' } }

    fun convertToString(): String {
        var result = ""
        image.forEach { result = result.plus(String(it)).plus("\n") }
        return result
    }

    fun clear() {
        image = Array(width) { CharArray(height) { ' ' } }
    }
}