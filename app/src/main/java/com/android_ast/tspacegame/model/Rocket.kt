package com.android_ast.tspacegame.model

class Rocket(var x: Int, var y: Int) {

    companion object {
        val image: Array<CharArray> = arrayOf(
            "A".toCharArray(),
            "!".toCharArray(),
            "M".toCharArray()
        )
    }
}