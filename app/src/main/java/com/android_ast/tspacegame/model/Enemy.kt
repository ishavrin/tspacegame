package com.android_ast.tspacegame.model

class Enemy(var x: Int, var y: Int) {
    val image: Array<CharArray> = arrayOf(
        "     ".toCharArray(),
        " ooo ".toCharArray(),
        "oWoWo".toCharArray(),
        "ooooo".toCharArray(),
        "\" \" \"".toCharArray()
    )
}