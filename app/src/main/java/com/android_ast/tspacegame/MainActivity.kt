package com.android_ast.tspacegame

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import com.android_ast.tspacegame.util.EnemyUtil.getAllFirst
import com.android_ast.tspacegame.model.*
import com.android_ast.tspacegame.util.Painter
import io.reactivex.Completable
import io.reactivex.Observable.interval
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue


class MainActivity : AppCompatActivity() {

    private val FRAME_PERIOD_MS = 1000L / 30L
    private val MOVE_PERIOD_MS = 600L
    private val PLAYER_MOVE_PERIOD_MS = 30L
    private val ROCKET_MOVE_PERIOD_MS = 20L
    private val BULLET_FIRE_PERIOD = 800L
    private val BULLET_MOVE_PERIOD = 150L
    private val BULLET_MOVE_STEP = 1
    private val BULLET_LIST_SIZE = 7
    private val ROCKET_MOVE_STEP = -1
    private var ENEMY_MOVE_SPEED = 2

    private val mediaPlayer = MediaPlayer(this)

    private val field = BattleField(40, 48)
    private var enemyList: ArrayList<Enemy> = ArrayList()
    private val playerHeight = field.width - Player.image.size / 5 * 8
    private var player = Player(playerHeight, field.height / 2 - Player.image.size / 2)
    private var isPlayer = true
    private var isRocket = true
    private val rocket = Rocket(playerHeight, field.height / 2 + 1)
    private var playerNapr = 0

    private val enemyBulletList: ArrayList<Bullet> = ArrayList()
    private var compositeDisposable = CompositeDisposable()

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createEnemiesDelayed()

        button_left.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> playerNapr = -1
                MotionEvent.ACTION_UP -> if (playerNapr == -1) playerNapr = 0
                else -> {
                }
            }
            true
        }

        button_right.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> playerNapr = 1
                MotionEvent.ACTION_UP -> if (playerNapr == 1) playerNapr = 0
                else -> {
                }
            }
            true
        }
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.stopFoneMusic()
        compositeDisposable.dispose()
        compositeDisposable.clear()
    }

    override fun onResume() {
        super.onResume()
        mediaPlayer.playFoneMusic()
        compositeDisposable = CompositeDisposable()
        compositeDisposable.add(interval(FRAME_PERIOD_MS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { draw() }
        )

        compositeDisposable.add(interval(MOVE_PERIOD_MS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { moveEnemy() }
        )
        compositeDisposable.add(interval(PLAYER_MOVE_PERIOD_MS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { movePlayer() }
        )
        compositeDisposable.add(interval(ROCKET_MOVE_PERIOD_MS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { moveRocket() }
        )
        compositeDisposable.add(interval(BULLET_FIRE_PERIOD, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { createBullet() }
        )
        compositeDisposable.add(interval(BULLET_MOVE_PERIOD, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { moveBullet() }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stopFoneMusic()
    }

    private fun moveBullet() {
        val bulletToDelete = ArrayList<Bullet>()
        enemyBulletList.forEach {
            if ((it.x + BULLET_MOVE_STEP) < field.width) {
                it.x += BULLET_MOVE_STEP
                if (checkBulletTarget(it.x, it.y))
                    bulletToDelete.add(it)
            } else {
                bulletToDelete.add(it)
            }
        }
        enemyBulletList.removeAll(bulletToDelete)
        checkBullets()
    }

    private fun checkBulletTarget(x: Int, y: Int): Boolean {
        if (!isPlayer) {
            return false
        }
        if (
            x >= player.x &&
            x <= (player.x + Player.image.size) &&
            y >= player.y &&
            y <= (player.y + Player.image.size)
        ) {
            isPlayer = false
            createPlayerDelayed()
            return true
        }
        return false
    }

    @SuppressLint("CheckResult")
    private fun createPlayerDelayed() {
        Completable.timer(2500, TimeUnit.MILLISECONDS)
            .subscribe {
                player = Player(playerHeight, field.height / 2 - Player.image.size / 2)
                isPlayer = true
            }
    }

    private fun createBullet() {
        if (enemyBulletList.size > BULLET_LIST_SIZE || !isPlayer) return
        val allFirstEnemies = getAllFirst(enemyList)
        if (allFirstEnemies.isEmpty()) return
        val y = (0 until allFirstEnemies.size).random()
        enemyBulletList.add(
            Bullet(
                allFirstEnemies[y].x + allFirstEnemies[y].image.size,
                allFirstEnemies[y].y + allFirstEnemies[y].image.size / 2
            )
        )
    }

    private fun moveRocket() {
        if ((rocket.x + ROCKET_MOVE_STEP) > 0) {
            rocket.x += ROCKET_MOVE_STEP
            checkTargets()
            checkBullets()
        } else {
            newRocket()
        }
    }


    private fun checkTargets() {
        val enemyListToRemove: ArrayList<Enemy> = ArrayList()
        if (enemyList.isEmpty() || !isRocket) return
        enemyList.forEach {
            if (
                rocket.x >= it.x &&
                rocket.x <= (it.x + it.image.size) &&
                rocket.y >= it.y &&
                rocket.y <= (it.y + it.image.size)
            ) {
                enemyListToRemove.add(it)
                newRocket()
                return@forEach
            }
        }
        enemyList.removeAll(enemyListToRemove)
        if (enemyList.isEmpty()) {
            createEnemiesDelayed()
        }
    }

    private fun checkBullets() {
        if (!isRocket) return
        val bulletListToRemove: ArrayList<Bullet> = ArrayList()
        if (enemyBulletList.isEmpty()) return
        enemyBulletList.forEach {
            if (rocket.x == it.x && rocket.y == it.y) {
                bulletListToRemove.add(it)
                newRocket()
                return@forEach
            }
        }
        enemyBulletList.removeAll(bulletListToRemove)
    }

    private fun newRocket() {
        isRocket = false
        if (!isPlayer) return
        rocket.x = playerHeight
        rocket.y = player.y + Player.image.size / 2 + 1
        isRocket = true
    }

    private fun movePlayer() {
        if ((player.y + playerNapr) > 0 && (player.y + playerNapr + Player.image.size + 1) < field.height)
            player.y += playerNapr
        checkCollisionWithPlayer()
    }

    private fun createEnemiesDelayed() {
        Log.d("rx", "createEnemiesDelayed")
        Completable.timer(1500, TimeUnit.MILLISECONDS)
            .subscribe {
                Log.d("rx", "createEnemies")
                createEnemies()
            }
    }

    private fun createEnemies() {
        enemyList = ArrayList()
        for (xx in 0..3) {
            for (yy in 0..5) {
                enemyList.add(Enemy(xx * 5, yy * 8))
            }
        }
        enemyList.sortWith(Comparator { e1: Enemy, e2: Enemy -> e1.y - e2.y })
    }

    private fun moveEnemy() {
        if (enemyList.isEmpty()) return
        if (((enemyList.last().y + ENEMY_MOVE_SPEED + enemyList.first().image.size) > field.height) || ((enemyList.first().y + ENEMY_MOVE_SPEED) < 0)) {
            ENEMY_MOVE_SPEED = -ENEMY_MOVE_SPEED
            enemyList.forEach { it.x += ENEMY_MOVE_SPEED.absoluteValue / 2 }
            return
        }
        enemyList.forEach {
            it.y += ENEMY_MOVE_SPEED

        }
        checkCollisionWithPlayer()
        if (enemyList.firstOrNull { (it.x + it.image.size) > field.width } != null) {
            newGame()
        }
    }

    private fun checkCollisionWithPlayer() {
        if (!isPlayer) return
        val enemiesCollision = ArrayList<Enemy>()
        enemyList.forEach {
            if ((player.x >= it.x &&
                        player.x <= (it.x + it.image.size) &&
                        player.y >= it.y &&
                        player.y <= (it.y + it.image.size))
                || (player.x + Player.image.size >= it.x &&
                        player.x + Player.image.size <= (it.x + it.image.size) &&
                        player.y >= it.y &&
                        player.y <= (it.y + it.image.size))
                || (player.x + Player.image.size >= it.x &&
                        player.x + Player.image.size <= (it.x + it.image.size) &&
                        player.y + Player.image.size >= it.y &&
                        player.y + Player.image.size <= (it.y + it.image.size))
                || (player.x >= it.x &&
                        player.x <= (it.x + it.image.size) &&
                        player.y + Player.image.size >= it.y &&
                        player.y + Player.image.size <= (it.y + it.image.size))
            )
                enemiesCollision.add(it)
        }
        if (enemiesCollision.isNotEmpty()) {
            enemyList.removeAll(enemiesCollision)
            if (enemyList.isEmpty()) {
                createEnemiesDelayed()
            }
            isPlayer = false
            createPlayerDelayed()
        }
    }

    private fun newGame() {
        enemyList.removeAll(enemyList)
        createEnemiesDelayed()
        isPlayer = false
        createPlayerDelayed()
    }

    private fun draw() {
        if (isPlayer) Painter.draw(field.image, Player.image, player.x, player.y)
        enemyList.forEach { Painter.draw(field.image, it.image, it.x, it.y) }
        if (isRocket) Painter.draw(field.image, Rocket.image, rocket.x, rocket.y)
        enemyBulletList.forEach { Painter.draw(field.image, Bullet.image, it.x, it.y) }
        mainText.text = field.convertToString()
        field.clear()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_SOFT_LEFT -> {
                playerNapr = -1
                return true
            }
            KeyEvent.KEYCODE_SOFT_RIGHT -> {
                playerNapr = 1
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}

