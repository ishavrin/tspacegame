package com.android_ast.tspacegame

import android.content.Context
import android.media.MediaPlayer


class MediaPlayer(internal var mContext: Context) {
    private var mMediaPlayer = MediaPlayer()

    fun stopFoneMusic() {
        try {
            mMediaPlayer.stop()
            mMediaPlayer.release()
        } catch (e: Exception) {
        }
    }

    fun playFoneMusic() {

        try {
            try {
                if (mMediaPlayer.isPlaying) {
                    mMediaPlayer.stop()
                    mMediaPlayer.release()
                    mMediaPlayer = MediaPlayer()
                } else {
                    mMediaPlayer = MediaPlayer()
                }
            } catch (e: Exception) {
                mMediaPlayer = MediaPlayer()
            }

            val descriptor = mContext.getAssets().openFd("x.mp3")
            mMediaPlayer.setDataSource(
                descriptor.getFileDescriptor(),
                descriptor.getStartOffset(),
                descriptor.getLength()
            )
            descriptor.close()

            mMediaPlayer.prepare()
            mMediaPlayer.setVolume(1f, 1f)
            mMediaPlayer.isLooping = true
            mMediaPlayer.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
