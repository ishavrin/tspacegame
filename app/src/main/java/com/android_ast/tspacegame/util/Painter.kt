package com.android_ast.tspacegame.util

import android.util.Log

object Painter {
    fun draw(canvas: Array<CharArray>, frame: Array<CharArray>, x: Int, y: Int) {
        for (xx in 0 until frame.size) {
            for (yy in 0 until frame[0].size) {
                try {
                    if (frame[xx][yy] == '1') {
                        canvas[xx + x][yy + y] = randomChar()
                    } else {
                        canvas[xx + x][yy + y] = frame[xx][yy]
                    }
                } catch (e: Exception) {
                    Log.e("${this.javaClass} draw", "$e")
                }


            }
        }
    }

    private fun randomChar(): Char {
        val r = (0..3).random()
        return when (r) {
            0 -> '$'
            1 -> '@'
            2 -> '%'
            else -> ' '
        }
    }
}