package com.android_ast.tspacegame.util

import com.android_ast.tspacegame.model.Enemy

object EnemyUtil {
    fun getVerticalRow(enemies: ArrayList<Enemy>): ArrayList<Enemy> {
        var row = ArrayList<Enemy>()
        return if (enemies.isEmpty()) row
        else ArrayList(enemies.filter { enemies[0].y == it.y })
    }

    fun getFirstInRow(row: ArrayList<Enemy>): Enemy? {
        return row.maxBy { it.x }
    }

    fun getAllFirst(enemies: ArrayList<Enemy>): ArrayList<Enemy> {
        var enemiesCopy: ArrayList<Enemy> = ArrayList(enemies)
        val allFirst = ArrayList<Enemy>()
        while (enemiesCopy.isNotEmpty()) {
            val row = getVerticalRow(enemiesCopy)
            getFirstInRow(row)?.let { allFirst.add(it) }
            enemiesCopy.removeAll(row)
        }
        return allFirst
    }
}